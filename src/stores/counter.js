import {defineStore} from 'pinia'
import axios from "axios";

export const useCounterStore = defineStore('counter', {
  state: () => ({
    ingredients: []
  }),
  actions: {
    async loadAllIngredients() {
      const x = {a:1, b:2, c: 3}
      const y = {a:1, b:2, c: 3}

      //const {a} = x
      const a = x.a

      //const {a: yA} = y
      const yA = y.a

      //const {data: ingredients} = await axios.get('https://dipbrpjcsi.recipes.asw.rest/api/ingredients')
      //this.ingredients = ingredients

      const ingredients = await axios.get('https://dipbrpjcsi.recipes.asw.rest/api/ingredients')
      console.log(ingredients)
      this.ingredients = ingredients.data
    }
  }
})
