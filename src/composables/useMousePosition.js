import {onBeforeUnmount, onMounted, ref} from "vue";

let a = ref(5);
let b = a;

a.value = 10;
// b.value -> 10

export function useMousePosition() {
    const mousePositionX = ref(0)
    const mousePositionY = ref(0)

    function setMousePosition(event) {
        console.log(event)
        mousePositionX.value = event.pageX
        mousePositionY.value = event.pageY
    }

    onMounted(() => {
        document.addEventListener('mousemove', setMousePosition)
    })

    onBeforeUnmount(() => {
        document.removeEventListener('mousemove', setMousePosition)
    })

    return {
        x: mousePositionX,
        y: mousePositionY
    }
}
